//
//  Game.hpp
//  neco
//
//  Created by Rintaro on 2016/07/04.
//
//

#ifndef Game_hpp
#define Game_hpp

#include "cocos2d.h"

class Game : public cocos2d::Layer
{
public:
    virtual bool init();
    static cocos2d::Scene* scene();
    CREATE_FUNC(Game);
};


#endif /* Game_hpp */
