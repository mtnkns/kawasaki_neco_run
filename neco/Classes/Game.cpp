//
//  Game.cpp
//  neco
//
//  Created by Rintaro on 2016/07/04.
//
//

#include "Game.hpp"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

Scene* Game::scene()
{
    Scene* scene = Scene::create();
    Game* Layer = Game::create();
    scene->addChild(Layer);
    return scene;
    
}
